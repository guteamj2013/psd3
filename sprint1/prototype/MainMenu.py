import ViewAttendance
import CSVAttendanceLogger
import ExportAttendance
import ExportStudent
import AttendanceRecording
import MockAuth
import db

def authenticate(user, pwd):
    userlist = ['admin', 'student', 'tutor', 'lecturer']
    if user in userlist and pwd == 'password': return True
    else: return False

logged_in = MockAuth.Log_in()
while logged_in!=True:
    logged_in = MockAuth.Log_in()

db = db.db
command = 'help'
help = "List of available commands:\n\
Type 'import attendance' or '1' to import attendance data from a file\n\
Type 'export attendance' or '2' to export all the attendance data fora  given course\n\
Type 'export attendance student' '3' to export attendance data on a given student\n\
Type 'record attendance' or '4' to record attendance for a given lab\n\
Type 'view attendance' or '5' to view attendance data\n\
Type 'log out' to log out\n\
Type 'exit' to quit\n\
Type 'help' for help\n\
       "

while command != 'exit':
    if command == 'help': print help
    elif command == 'exit': quit()
    elif command == 'log out':
        logged_in = MockAuth.Log_in()
        while logged_in!=True:
             logged_in = MockAuth.Log_in()
                        #import attendance
    elif command == 'import attendance' or command == '1':
            CSVAttendanceLogger.log_attendance()
                        #export attendance
    elif command == 'export attendance' or command == '2':
            cID = raw_input('Enter course ID: ')
            ExportAttendance.exportAttendance(cID)

                        #export student
    #elif command == 'export student' or command == '3':
     #       stID = input('Enter student ID: ')
      #      ExportStudent.exportAllInfo(stID)

                        #export student
    elif command == 'export attendance student' or command == '3':
            stID = input('Enter student ID: ')
            ExportStudent.exportAttendanceStudent(stID)

                            #record attendance
    elif command == 'record attendance' or command == '4':
            AttendanceRecording.AttendanceRecording()
                            #view attendance data
    elif command == 'view attendance' or command == '5':
            ViewAttendance.viewAttendance()

    else: print 'Command not found'
    command = raw_input('Enter command (enter "help" for help): ')

