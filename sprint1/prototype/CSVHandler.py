import csv


class CSVHandler(object):
    def __init__(self, delimiter=None, quotechar=None):
        self.delimiter = delimiter if delimiter else ','
        self.quotechar = quotechar if quotechar else '|'

    def load_csv(self, filename):
        """Returns the rows of the csv as a list of lists"""

        try:
            with open(filename, 'r') as csvfile:
                filereader = csv.reader(csvfile, delimiter=self.delimiter, quotechar=self.quotechar)
                return [row for row in filereader]
        except:
            return []

    def export_csv(self, data, filename):
        """ Exports the data (a list of lists) to csv format"""

        with open(filename, 'w') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=self.delimiter, quotechar=self.quotechar,
                                    quoting=csv.QUOTE_MINIMAL)
            for row in data:
                filewriter.writerow(row)


class UniversityCSVHandler(CSVHandler):
    def __init__(self):
        super(CSVHandler, self).__init__()
        self.delimiter = ','
        self.quotechar = '|'

    def export_course(self, data, course):
        """ Exports data (all student attendance records for a single course) to csv """
        sessions = []
        students = {}
        for record in data:
            if record['name'] not in sessions:
                sessions += [record['name']]

        for record in data:
            if record['ID number'] not in students:
                students[record['ID number']] = {'last': record['surname'],
                                                 'first': record['firstname'],
                                                 'attendance': []}
                for i in sessions:
                    students[record['ID number']]['attendance'] += ['absent']
            else:
                students[record['ID number']]['attendence'][sessions.index(record['name'])] = record['attendance']

        formatted_data = [['First name', 'Surname', 'ID number'] + sessions] + [
            [students[id]['first'], students[id]['last'], id] + [students[id]['attendance'][sessions.index(session)]] for id in students for session in sessions]
        self.export_csv(formatted_data, course + ".csv")

    def export_student(self, data, student_id):
        """ Exports data (all recorded information for a single student) to csv"""
        data_formatted = [['Course', 'CourseID', 'Session Timestamp', 'Attendance']]
        for course in data:
            for session in course['attendance']:
                data_formatted += [[course['course'], course['courseID'], session, course['attendance'][session]]]
        self.export_csv(data_formatted, str(student_id) + ".csv")
    
