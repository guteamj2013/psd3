import db
from CSVHandler import *

def exportAttendance(course):
    attList = []
    try:
        labs = db.get_course(course)["labs"]
        for stID in db.get_course(course)["students"]:
            firstname = db.get_student(stID)["name"]
            surname = db.get_student(stID)["surname"]

            for lab in labs.keys():
                if stID in labs[lab]["students"]:
                    att = labs[lab]["attendance"]
                    for atr in att.values():
                        if stID in atr["present"]:
                            attList.append({"firstname":firstname,"surname":surname,"ID number":stID, "name": atr["name"], "attendance":"present"})
                        elif stID in atr["absent"]:
                            attList.append({"firstname":firstname,"surname":surname,"ID number":stID,  "name": atr["name"], "attendance":"absent"})
                        elif stID in atr["mv"]:
                            attList.append({"firstname":firstname,"surname":surname,"ID number":stID, "name": atr["name"], "attendance":"mv"})


    except:
        print "course", course, "not found"
    handler = UniversityCSVHandler()
    handler.export_course(attList, course)

