import db

def make_selection_menu(list):
   index = 0
   print '-------------'
   for i in list:
    index = index +1
    print '(',index,')' ,i
   print '-------------'
   try:
    sel = input('Make a selection: (0 quit) ')
   except:
    print "\nwrong input\n"
    sel = make_selection_menu(list)
    return sel
   if (sel>index or sel <0):
    try:
     print 'Incorrect selection. try again: '
     sel = make_selection_menu(list)
     return sel
    except:
     print "\nwrong input\n"
     sel = make_selection_menu(list)
     return sel
   return sel

def create_or_choose(s):
 print
 try:
  sel = input(s)
 except:
  print "\nwrong input\n"
  sel = create_or_choose(s)
  return sel
 if (sel > 2 or sel < 1):
  try:
   print 'Incorrect selection. try again: '
   sel = create_or_choose(s)
   return sel
  except:
   print "\nwrong input\n"
   sel = create_or_choose(s)
 return sel

def ask_question(s):
 answer = raw_input(s)
 if answer == 'y' or answer =='n':
     return answer
 else:
     print '\nOOps... something wen wrong. Again..'
     answer = ask_question(s)
 
 

def recordLabAttendance(cID):
 index = -1
 lab_groups = db.get_course(cID)['labs'].keys()
 if len(lab_groups) ==0:
     print 'No labs'
     return
 labGroup = lab_groups[make_selection_menu(lab_groups)-1]
 if labGroup ==0:
     print 'Bye!'
     return

 sel = create_or_choose('Do you want to create a new session(1) or choose from existing(2)? ')
 lab_times = db.get_course_lab(cID,labGroup)['attendance'].keys()

 if len(lab_times)==0:
     print 'No sessions'
     return
 if sel == 1:
  date = raw_input('specify date (dd/mm/yyyy): ')
  time = raw_input('specify time (hh): ')
  new_session_timestamp = date + " %s:00" % time
  db.course_add_lab_session(cID,labGroup,'name??',date,time)
  print 'Created a new lab session'
  lab_times = db.get_course_lab(cID,labGroup)['attendance'].keys()
  index = lab_times.index(new_session_timestamp)

 else: 
  sel = make_selection_menu(lab_times)
  index = sel-1

 print 'altering ', db.get_course_lab(cID,labGroup)['attendance'].keys()[index]
 if sel != 0:

  present_list = db.get_course_lab(cID,labGroup)['attendance'][lab_times[index]]['present']
  absent_list = db.get_course_lab(cID,labGroup)['attendance'][lab_times[index]]['absent']
  mv_list = db.get_course_lab(cID,labGroup)['attendance'][lab_times[index]]['mv']
     
  for j in db.get_course_lab(cID,labGroup)['students']:
   inp = ask_question('Did student %s attend the lecture? (y/n): ' % j)
   if inp == 'y': 
    if j not in present_list:
     present_list.append(j) 
     if j in absent_list:
      absent_list.remove(j)
     if j in mv_list:
      mv_list.remove(j)
   else:
    ask = ask_question('Was absence with a good cause? (y/n): ')
    if ask == 'y':
     if j not in mv_list:
      mv_list.append(j)
     if j in absent_list:
      absent_list.remove(j)
     if j in present_list:
      present_list.remove(j)
    else:
     if j not in absent_list:
      absent_list.append(j)
     if j in present_list:
      present_list.remove(j)
     if j in mv_list:
      mv_list.remove(j)
  print 'Attendance recording done!'
  db.save_db_to_file()

 else:
  print 'Bye!' 


def recordLectureAttendance(cID):
 index = -1
 times = db.get_course(cID)['attendance'].keys()
 if len(times)==0:
     print 'No sessions!'
     return
 sel = create_or_choose('Do you want to create a new session(1) or choose from existing(2)? ')
 
 if sel == 1:
  date = raw_input('specify date: ')
  time = raw_input('specify time: ')
  new_session_timestamp = date + " %s:00" % time
  db.course_add_session(cID,date,time)
  print 'Created a new lecture session'
  times = db.get_course(cID)['attendance'].keys()
  index = times.index(new_session_timestamp)

 else: 
  sel = make_selection_menu(times)
  index = sel-1
 
 if sel != 0:
  attended_list = db.get_course(cID)['attendance'][times[index]]
  for j in db.get_course(cID)['students']:
   inp = ask_question('Did student %s attend the lecture? (y/n): ' % j)
   if inp == 'y': 
    if j not in attended_list:
     attended_list.append(j)
   elif j in attended_list:
    attended_list.remove(j)
  print 'Attendance recording done!'
  db.save_db_to_file()
 else:
  print 'Bye!'




def select(cID):
 try:
  sel = input('Record lecture(1) or lab(2) atendance? (0 for quit): ')
 except:
     print '\nwrong input!\n'
     select(cID)
     return
 if sel == 1:
  recordLectureAttendance(cID)
 elif sel == 2: 
  recordLabAttendance(cID)
 elif sel == 0: 
  print 'Bye!'
 else:
  print 'Incorrect selection!'
  select(cID)




def AttendanceRecording():
 courseName = make_selection_menu(db.db['courses'].keys())
 if courseName == 0:
  print 'Bye!'
 else:  
  cID = db.db['courses'].keys()[courseName-1]
  print 'Course selected: ',cID,'\n'
  select(cID)
  
