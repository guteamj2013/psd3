from CSVHandler import *
import db


def exportAllInfo(stID):
    stInfo = {}
    try:
        StudentId = stID
        FirstName = db.get_student(stID)["name"]
        LastName = db.get_student(stID)["surname"]
        courses = db.get_student(stID)["courses"]
        attendance = {}

        for c in courses:
            attdict = {}
            for st in db.get_course(c)["attendance"]:


                if StudentId in db.get_course(c)["attendance"].get(st):
                    attdict.setdefault(st, "present")
                    attendance.setdefault(c, attdict)

                else:
                    attdict.setdefault(st, "absent")
                    attendance.setdefault(c, attdict)

        stInfo = {"StudentID": StudentId, "Firstname": FirstName, "Lastname": LastName, "courses": courses,
                  "attendance": attendance}

    except:
        print "studentID", stID, "not found"
    handler = UniversityCSVHandler()
    handler.export_student(stInfo, stID)


def exportAttendanceStudent(stID):
    attInfo = []
    try:
        courses = db.get_student(stID)["courses"]
        for c in courses:

            for st in db.get_course(c)["attendance"]:
                attendance = {}

                if stID in db.get_course(c)["attendance"].get(st):
                    attendance.setdefault(st, "present")
                else:
                    attendance.setdefault(st, "absent")
                attInfo.append({"course": c, "courseID": db.get_course(c)["coursename"], "attendance": attendance})

    except:
        print "studentID", stID, "not found"

    handler = UniversityCSVHandler()
    handler.export_student(attInfo, stID)




    

