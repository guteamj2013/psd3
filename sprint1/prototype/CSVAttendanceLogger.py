from CSVHandler import CSVHandler
import db
import random as r


STUDENT_IDS = [
    '22222222',
    '66666666',
    '88888888',
]

class AttendanceCSVLogger(object):
    def __init__(self):
        self.handler = CSVHandler(delimiter=',')

    def __parse_csv(self, filename):
        """load attendance data and return as a structure suitable for quick DB entry"""
        data = self.handler.load_csv(filename)
        return [self.__student_id_lookup(row[0]) for row in data]

    def __input_to_db(self, data, course, lab_id):
        """take data and store in the database"""
        db.get_course_lab(course, lab_id)['attendance'] = data
        db.save_db_to_file()

    def __student_id_lookup(self, id):
        """extract the relevant student ID from the DB for the id code"""
        return STUDENT_IDS[r.randint(0, 2)]

    def log_attendance(self, filename, course, lab_id, lab_name, timestamp):
        """log the attendance records stored in the file for the given course"""
        present_students = self.__parse_csv(filename)
        all_lab_students = present_students  # REPLACE THIS
        attendance_dict = {timestamp: {'name': lab_name,
                                       'present': present_students,
                                       'absent': [student for student in all_lab_students if student not in present_students],
                                       'mv': [],
                                       },
                           }
        self.__input_to_db(data=attendance_dict, course=course, lab_id=lab_id)
        return attendance_dict

    def overwrite_attendance(self, course, lab, attendance):
        """log the attendance dict for the lab for the course"""
        self.__input_to_db(data=attendance, course=course, lab_id=lab)


def log_attendance():
    """function for main prog to call, will get users input and log attendance"""
    filename = raw_input("Please enter the name of the file containing attendance records: ")
    course_id, lab_id, lab_name, timestamp = raw_input("Please enter the course id, lab id, lab 'assignment' name and lab timestamp (format: cid, lid, labname, timestamp[dd/mm/yyyy HH:MM]): ").split(", ")
    attendance_logger = AttendanceCSVLogger()
    attendance_data = attendance_logger.log_attendance(filename=filename, course=course_id, lab_id=lab_id, lab_name=lab_name, timestamp=timestamp)
    mv = raw_input("Please enter any medically absent students (separated by a comma and space): ").split(", ")
    if mv:
        attendance_data[timestamp]['mv'] = mv
        attendance_data[timestamp]['absent'] = [student for student in attendance_data[timestamp]['absent'] if student not in mv]
        attendance_logger.overwrite_attendance(course=course_id, lab=lab_id, attendance=attendance_data)
    print "Attendance data logged. \nReturning to main menu...\n"
        
    



