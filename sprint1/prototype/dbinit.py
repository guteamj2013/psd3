import pickle
db = {"admins": {}, "students": {}, "tutors": {}, "lecturers": {}, "courses": {}, "users": {}}


def add_admin(uid, name, surname):
    db["admins"][uid] = {"name": name, "surname": surname}


def add_tutor(uid, name, surname):
    db["tutors"][uid] = {"name": name, "surname": surname}


def add_lecturer(uid, name, surname):
    db["lecturers"][uid] = {"name": name, "surname": surname}


def add_student(uid, name, surname):
    db["students"][uid] = {"name": name, "surname": surname, "courses": []}


def add_user(user, psw):
    db["users"][user] = psw


def change_psw(user, old_psw, new_psw):
    try:
        if old_psw == db["users"][user]:
            db["users"][user] = new_psw
        else:
            print "wrong password"
    except:
        print "user",user, "does not exist"


def student_add_course(uid, course):
    try:
        db["students"][uid]["courses"].append(course)
    except:
        print "student", uid, "not found"

def student_course(sid, cid):
    try:
        db["students"][sid]["courses"].append(cid)
    except:
        print "student", sid, "not found"
    try:
        db["courses"][cid]["students"].append(sid)
    except:
        print "course", cid, "not found"


def add_course(cid, coursname):
    db["courses"][cid] = {"coursename": coursname, "time": [], "students": [], "lecturers": [], "labs": {},
                          "attendance": {}}


def course_add_time(cid, day, time):
    try:
        db["courses"][cid]["time"].append(day + " %s:00" % time)
    except:
        print "course", cid, "not found"


def course_add_lecturer(cid, uid):
    try:
        db["courses"][cid]["lecturers"].append(uid)
    except:
        print "course", cid, "not found"


def course_add_student(cid, sid):
    try:
        db["courses"][cid]["students"].append(sid)
    except:
        print "course", cid, "not found"



def course_add_session(cid, date, time):
    try:
        db["courses"][cid]["attendance"][date + " %s:00" % time] = []
    except:
        print "course", cid, "not found"


def course_add_attendance(cid, date, time, student):
    try:
        db["courses"][cid]["attendance"][date + " %s:00" % time].append(student)
    except:
       print "course", cid, "or session not found"


def course_add_lab(cid, labgroup):
    try:
        db["courses"][cid]["labs"][labgroup] = {"tutors": [], "students": [], "time": [], "attendance": {}}
    except:
        print "course", cid, "not found"


def course_add_lab_tutor(cid, labgroup, tutor):
    try:
        db["courses"][cid]["labs"][labgroup]["tutors"].append(tutor)
    except:
        print "course", cid, "not found"


def course_add_lab_student(cid, labgroup, student):
    try:
        db["courses"][cid]["labs"][labgroup]["students"].append(student)
    except:
        print "course", cid, "not found"


def course_add_lab_time(cid, labgroup, day, time):
    try:
        db["courses"][cid]["labs"][labgroup]["time"].append(day + " %s:00" % time)
    except:
        print "course", cid, "not found"


def course_add_lab_session(cid, labgroup, name, date, time):
    try:
        db["courses"][cid]["labs"][labgroup]["attendance"][date + " %s:00" % time] = {"name": name, "present": [], "absent": [], "mv": []}
    except:
        print "course", cid, "or lab group", labgroup, "not found"

def course_add_lab_attendance(cid, labgroup, date, time, sid, status):
    try:
        db["courses"][cid]["labs"][labgroup]["attendance"][date + " %s:00" % time][status].append(sid)
    except:
        print "course", cid, "or lab group", labgroup, "not found"


def get_student(sid):
    return db["students"][sid]


def get_tutor(tid):
    return db["tutors"][tid]


def get_lecturer(lid):
    return db["lecturer"][lid]


def get_admin(aid):
    return db["admins"][aid]


def get_course(cid):
    return db["courses"][cid]


def get_course_lab(cid, labgroup):
    return db["courses"][cid]["labs"][labgroup]

def get_users():
    return db["users"].keys()

def get_user_psw(sid):
    return db["users"][sid]

def save_db_to_file():
    output = open("db-file.pkl", "wb")
    pickle.dump(db, output)
    output.close()


def load_db_from_file():
    pkl_file = open("db-file.pkl", "rb")
    global db
    db = pickle.load(pkl_file)
    pkl_file.close()

# tsts
add_course("COMPSCI 2001", "JP2")
add_admin(11111111, "Admin", "Boss")
add_user("admin", "password")
add_student(22222222, "Simple", "Student")
student_add_course(22222222, "COMPSCI 2001")
course_add_student("COMPSCI 2001", 22222222)
add_user("22222222s", "password")
add_lecturer(33333333, "Cool", "Lecturer")
add_user("clecturer", "password")
add_tutor(44444444, "Angry", "Tutor")
add_user("44444444t", "password")
add_lecturer(55555555, "Grumpy", "Lec")
add_user("glec", "password")
add_student(66666666, "Simple2", "Student2")
student_course(66666666, "COMPSCI 2001")
add_student(77777777, "Simple3", "Student3")
student_course(77777777, "COMPSCI 2001")
add_student(88888888, "Simple4", "Student4")
student_course(88888888, "COMPSCI 2001")
add_student(99999999, "Simple5", "Student5")
student_course(99999999, "COMPSCI 2001")
course_add_lecturer("COMPSCI 2001", 55555555)
course_add_time("COMPSCI 2001", "Wed", 10)
course_add_time("COMPSCI 2001", "Fri", 10)
course_add_session("COMPSCI 2001", "13/11/2013", 10)
course_add_session("COMPSCI 2001", "20/11/2013", 10)
course_add_session("COMPSCI 2001", "24/11/2013", 10)
course_add_session("COMPSCI 2001", "13/11/2013", 10)
course_add_attendance("COMPSCI 2001", "13/11/2013", 10, 22222222)
course_add_attendance("COMPSCI 2001", "20/11/2013", 10, 22222222)
course_add_attendance("COMPSCI 2001", "24/11/2013", 10, 22222222)
course_add_attendance("COMPSCI 2001", "13/11/2013", 10, 99999999)
course_add_lab("COMPSCI 2001", "A")
course_add_lab_time("COMPSCI 2001", "A", "Fri", 13)
course_add_lab_session("COMPSCI 2001", "A", "Ex1", "13/11/2013", 13)
course_add_lab_student("COMPSCI 2001", "A", 22222222)
course_add_lab_student("COMPSCI 2001", "A", 77777777)
course_add_lab_student("COMPSCI 2001", "A", 99999999)
course_add_lab_tutor("COMPSCI 2001", "A", 44444444)
course_add_lab_attendance("COMPSCI 2001", "A", "13/11/2013", 13, 22222222, "present")
course_add_lab_attendance("COMPSCI 2001", "A", "13/11/2013", 13, 99999999, "mv")
course_add_lab_attendance("COMPSCI 2001", "A", "13/11/2013", 13, 77777777, "absent")

save_db_to_file()
load_db_from_file()

#print db
