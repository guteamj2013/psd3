from prototype.CSVHandler import CSVHandler
import unittest

test_data = [['John', '1103440f'],
                      ['Mary', '3340540d']]

class TestCSVHandler(unittest.TestCase):
    
    
    def setUp(self):
        self.handler = CSVHandler(delimiter=',', quotechar='|')
        
    def test_import(self):
        self.assertEqual(test_data, self.handler.load_csv("mock_data.csv"), "Imported data not correct")
        
    def test_export(self):
        """This could be better - if do not consider a successful run of this test ass
           true unless test_import also passed"""
        
        self.handler.export_csv(test_data, "test_export.csv")
        self.assertEqual(test_data, self.handler.load_csv("mock_data.csv"), "CSV exporting does not function correctly")



if __name__ == '__main__':
    unittest.main()