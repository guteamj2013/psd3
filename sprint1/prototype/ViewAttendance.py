import db

def viewAttendance():
    option = raw_input('Lecture or lab attendance? [lec/lab]: ')
    if option == 'lab':
        try:
            cID = raw_input('Enter course ID: ')
            lGroup = raw_input('Enter lab group: ')
            timestamp = raw_input('Enter date and time of the lab [dd/mm/yyyy hh:mm]: ')
            attendance = db.get_course_lab(cID, lGroup)['attendance'][timestamp]
            print 'Present:',
            for student in attendance['present']: print str(student) + ',',
            print
            print 'Absent:',
            for student in attendance['absent']: print str(student) + ',',
            print
        except:
            opt = raw_input('Course, lab, or session does not exist. Try again? [y/n]: ')
            if opt == 'y':
                viewAttendance()
            else: return

    elif option == 'lec':
        try:
            cID = raw_input('Enter course ID: ')
            timestamp = raw_input('Enter date and time of the lecture [dd/mm/yyyy hh:mm]: ')
            attendance = db.get_course(cID)['attendance'][timestamp]
            print 'Present:',
            for student in attendance: print str(student) + ',',
            print
        except:
            opt = raw_input('Course or session does not exist. Try again? [y/n]: ')
            if opt == 'y':
                viewAttendance()
            else: return
    else: viewAttendance()