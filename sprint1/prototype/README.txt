TEAM J SPRINT 1 PROTOTYPE

BUILD & RUN INSTRUCTIONS.
System Requirements:
Python 2.7

Running System.
To setup the database initially run "python dbinit.py", and then run "python MainMenu.py" to initiate the program.
For testing of the system, please use the credentials username: "admin", password: "password"

TESTING INSTRUCTIONS
USER STORY 1:
To export attendance for a course select the "export attendance" menu option, and follow the instructions using Course ID COMPSCI 2001, to export a CSV for the example course.
To export attendance for a student select the "export attendance student" menu option, and follow the instructions using Student Id 22222222. 

USER STORY 2:
To record attendance manually, select the "record attendance" menu option and follow the on screen instructions.
To record attendance by importing from CSV, slect the "import attendance" menu option, enter BarcodeData.csv as the filename and follow the onscreen instructions.
To view the attendance for a course, select the "view attendance" menu option, and use the following input: lab [enter] COMPSCI 2001 [enter] A [enter] 13/11/2013 13:00 [enter] where [enter] signifies pressing the enter key.