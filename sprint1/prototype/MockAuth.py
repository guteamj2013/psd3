import db
import sys

def Log_in():
 #print db.db['users']
 users = db.get_users()
 #print users

 id = raw_input('\nPlease enter your user id (0 to quit): ')
  
 if id == "0":
  sys.exit(0)
 
 if (id in users):
  passw = raw_input('Enter your password: ')

  if passw == db.get_user_psw(id):
   print'Log in successful, welcome',id
   return True  

  else:
   print 'password does not match, log in failed.'
   return False
 else:
  print 'Log in not found!'
  return Log_in()
