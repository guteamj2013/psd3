SPRINT 2 README

Please note that this prototype has incomplete functionality.

SYSTEM REQUIREMENTS
Java
Apache Maven
A web browser

BUILD & RUN INSTRUCTIONS
to build the package, run "mvn clean package"
to run the system, run "java -jar target/psd3Prototype2-1.jar"

TESTING INSTRUCTIONS
Unfortunately, due the prototype being incomplete, testing the user stories is impossible as the system will only give HTTP 404 responses.
However, it is possible to open the html files in a web browser to see what each stage would have looked like.
