

import java.util.*;
import java.text.*;
import com.groupj.psd3.db.*;

class FetchData{
	public class SesNode{
		String course;
		String type;
		String date;
		String description;
		String location;

		public SesNode(String c, String t, String dt, String desc, String loc){
			course = c;
			type = t;
			date = dt;
			description = desc;
			location = loc;
		}

		public String getCourse(){return course;}
		public String getType(){return type;}
		public String getDate(){return date;}
		public String getDescription(){return description;}
		public String getLocation(){return location;}
	}
	//returns true if date is in current week
	public boolean inCurrentWeek(Date date) {
  		Calendar currentCalendar = Calendar.getInstance();
  		int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
  		int year = currentCalendar.get(Calendar.YEAR);
  		Calendar targetCalendar = Calendar.getInstance();
  		targetCalendar.setTime(date);
  		int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
  		int targetYear = targetCalendar.get(Calendar.YEAR);
  		return week == targetWeek && year == targetYear;
}

	public LinkedList<SesNode> fetchData(String id, String range){
		SimpleDateFormat dateForm = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		Database database = Database.getDB();
		LinkedList<SesNode> data = new LinkedList<SesNode>();
		Date today = calendar.getTime();
		Student user = database.getStudent(id);
		HashSet<Course> courses = user.getCourses();
		Iterator<Course> itCourses = courses.iterator();
 		
 		//iterate over users courses
		while(itCourses.hasNext()){
			Course course = itCourses.next();
			HashSet<Lab> labs = course.getLabs();
			HashSet<Lecture> lectures = course.getLectures();
			Iterator<Lab> itLabs = labs.iterator();
			Iterator<Lecture> itLecs = lectures.iterator();

			//iterate over labs
			while(itLabs.hasNext()){
				Lab lab = itLabs.next();
				LinkedHashSet<Session> sessions = lab.getSessions();
				Iterator<Session> itSessions = sessions.iterator();

				//iterate over sessions
				while(itSessions.hasNext()){
					Session session = itSessions.next();
					if (range.equals("today")){
 						 if (dateForm.format(today).equals(dateForm.format(session.getDate()))){
 						 	data.add(new SesNode(course.getCourseName(), "lab", dateForm.format(session.getDate()),
 						 		session.getDescription(), session.getLocation()));
 						 }
					}
					else if (range.equals("week")){
						if (inCurrentWeek(session.getDate())){
							data.add(new SesNode(course.getCourseName(), "lab", dateForm.format(session.getDate()),
 						 		session.getDescription(), session.getLocation()));
						}
					}
					else if (range.equals("forever")){
						data.add(new SesNode(course.getCourseName(), "lab", dateForm.format(session.getDate()),
 						 		session.getDescription(), session.getLocation()));
					}
				}
			}

			//iterate over lectures
			while(itLecs.hasNext()){
				Lecture lec= itLecs.next();
				LinkedHashSet<Session> sessions = lec.getSessions();
				Iterator<Session> itSessions = sessions.iterator();

				//iterate over sessions
				while(itSessions.hasNext()){
					Session session = itSessions.next();
					if (range.equals("today")){
 						 if (dateForm.format(today).equals(dateForm.format(session.getDate()))){
 						 	data.add(new SesNode(course.getCourseName(), "lab", dateForm.format(session.getDate()),
 						 		session.getDescription(), session.getLocation()));
 						 }
					}
					else if (range.equals("week")){
						if (inCurrentWeek(session.getDate())){
							data.add(new SesNode(course.getCourseName(), "lab", dateForm.format(session.getDate()),
 						 		session.getDescription(), session.getLocation()));
						}
					}
					else if (range.equals("forever")){
						data.add(new SesNode(course.getCourseName(), "lab", dateForm.format(session.getDate()),
 						 		session.getDescription(), session.getLocation()));
					}
				}
			
			}
		}	
	return data;
	}


}