

import com.groupj.psd3.db.Database;
import com.groupj.psd3.db.User2;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.ui.Model;

// Lacks database package import, if it's not in the same package.
import db.*;


@Controller
public class Login {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String ShowSignIn(Model model) {
		return "signin";
	}

	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public String SignIn(
			@RequestParam(value = "username", required = true) String id,
			@RequestParam(value = "password", required = true) String pass,
			Model model) {
		Database db = Database.getDB();
		User2 user;
		try{
			user = db.getUser(id);
		}catch (Exception e) { return "signin";}
		if (user.authenticate(pass)) {
			model.addAttribute("user", user);	//adds the user instance to the model
			model.addAttribute("password", pass);	//adds the entered password to the model
			return "index";
		}
		return "signin";
	}
	
	//controller to take in and pass back the user instance and the entered password to the newrecord.html
	@RequestMapping(value = "/newrecord", method = RequestMethod.POST)
	public String PassOver(
			@RequestParam(value = "user", required = true) User2 user,
			@RequestParam(value = "password", required = true) String pass,
			Model model) {
		model.addAttribute("user", user);
		model.addAttribute("password", pass);
		return "newrecord";
	}
	
	

}
