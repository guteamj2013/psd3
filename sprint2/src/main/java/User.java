

public class User {

	private String id, pass, level = null;
	private boolean success = false;

	protected String getId() {
		return id;
	}

	protected boolean authenticate() {
		if (id.matches("admin") && pass.matches("admin")) {
			success = true;
			level = "admin";
			return success;
		}
		if (id.matches("user") && pass.matches("user")) {
			success = true;
			level = "user";
			return success;
		}
		level = null;
		return success = false;
		
	}

	protected void setId(String id) {
		this.id = id;
	}

	protected String getPass() {
		return pass;
	}

	protected void setPass(String pass) {
		this.pass = pass;
	}

	protected String getLevel() {
		return level;
	}

	protected void setLevel(String level) {
		this.level = level;
	}

	protected boolean isSuccess() {
		return success;
	}

	protected void setSuccess(boolean success) {
		this.success = success;
	}
}
