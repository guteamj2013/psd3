package com.groupj.psd3.db;


import java.io.Serializable;
import java.util.HashSet;

public abstract class Person implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	protected String email;
	private String name;
	private String surname;
	protected HashSet<Course> courses;

	public Person(String id, String name, String surname) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.courses = new HashSet<Course>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public HashSet<Course> getCourses() {
		return courses;
	}

	public void setCourses(HashSet<Course> courses) {
		this.courses = courses;
	}

	void addCourse(Course course, boolean db) {
		courses.add(course);
	}

	public void addCourse(Course course) {
		addCourse(course, true);
		if (this instanceof Student)
			Database.getDB().getCourse(course.getCourseID())
					.addStudent((Student) this, true);
		else if (this instanceof Tutor)
			Database.getDB().getCourse(course.getCourseID())
					.addTutor((Tutor) this, true);
		else if (this instanceof Lecturer)
			Database.getDB().getCourse(course.getCourseID())
					.addLecturer((Lecturer) this, true);
		else if (this instanceof Admin)
			Database.getDB().getCourse(course.getCourseID())
					.addAdmin((Admin) this, true);

	}

}
