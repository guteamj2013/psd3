package com.groupj.psd3.db;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashSet;

public class Lab implements Serializable {

	private static final long serialVersionUID = 1L;
	private LinkedHashSet<Session> sessions;
	private HashSet<Student> students;
	private Course course;
	private String group;
	private String day;
	private String time;
	private int capacity;
	private Tutor tutor;

	public Lab(LinkedHashSet<Session> sessions, HashSet<Student> students,
			Course course, String group, String day, String time, int capacity) {
		super();
		this.sessions = sessions;
		this.students = students;
		this.course = course;
		this.group = group;
		this.day = day;
		this.time = time;
		this.capacity = capacity;
	}

	public Lab(String group, String day, String time, int capacity) {
		super();
		this.sessions = new LinkedHashSet<Session>();
		this.students = new HashSet<Student>();
		this.course = null;
		this.group = group;
		this.day = day;
		this.time = time;
		this.capacity = capacity;
	}

	public LinkedHashSet<Session> getSessions() {
		return sessions;
	}

	public void setSessions(LinkedHashSet<Session> sessions) {
		this.sessions = sessions;
	}

	public HashSet<Student> getStudents() {
		return students;
	}

	public void setStudents(HashSet<Student> students) {
		this.students = students;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Tutor getTutor() {
		return tutor;
	}

	public void setTutor(Tutor tutor) {
		this.tutor = tutor;
	}

}
