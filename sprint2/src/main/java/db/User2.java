package com.groupj.psd3.db;

import java.io.Serializable;

public class User2 implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private String pass;
	private int level; // 0 - student, 1 - tutor, 2 - lecturer, 3 - admin, -1 -
						// no access
	private Person person;

	public User2(String username, String pass, int level, Person person) {
		this.username = username;
		this.pass = pass;
		this.level = level;
		this.person = person;
	}

	public User2(String username, String pass, Person person) {
		this.username = username;
		this.pass = pass;
		if (person instanceof Student)
			this.level = 0;
		else if (person instanceof Tutor)
			this.level = 0;
		else if (person instanceof Lecturer)
			this.level = 1;
		else if (person instanceof Admin)
			this.level = 3;
		else
			this.level = -1;
		this.person = person;
	}

	public User2(String username, String pass, int level) {
		this.username = username;
		this.pass = pass;
		this.level = level;
		this.person = null;
	}

	public User2(String username, String pass) {
		this.username = username;
		this.pass = pass;
		this.level = 0;
		this.person = null;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	private String getPass() {
		return pass;
	}

	private void setPass(String pass) {
		this.pass = pass;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public boolean authenticate(String psw) {
		if (pass.equals(psw))
			return true;
		else
			return false;
	}

	public boolean changePsw(String oldpsw, String newpsw) {
		if (pass.equals(oldpsw)) {
			setPass(newpsw);
			return true;
		} else
			return false;
	}

}
