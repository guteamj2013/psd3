package com.groupj.psd3.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

public class Database implements Serializable {

	private static final long serialVersionUID = 1L;
	private static Database database = null;
	@SuppressWarnings("rawtypes")
	private HashMap<String, HashMap> db;

	@SuppressWarnings("rawtypes")
	private Database() {
		db = new HashMap<String, HashMap>(6, (float) 1.0);
		HashMap<String, Admin> admins = new HashMap<String, Admin>();
		db.put("admins", admins);
		HashMap<String, Student> students = new HashMap<String, Student>();
		db.put("students", students);
		HashMap<String, Tutor> tutors = new HashMap<String, Tutor>();
		db.put("tutors", tutors);
		HashMap<String, Lecturer> lecturers = new HashMap<String, Lecturer>();
		db.put("lecturers", lecturers);
		HashMap<String, Course> courses = new HashMap<String, Course>();
		db.put("courses", courses);
		HashMap<String, User2> users = new HashMap<String, User2>();
		db.put("users", users);
	}

	public static Database getDB() {
		if (database == null) {
			database = loadDB();
		}
		return database;
	}

	public static Database getNewDB() {
		if (database == null) {
			database = new Database();
		}
		return database;
	}

	public static void saveDB() {
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(new FileOutputStream("db.ser"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			oos.writeObject(database);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static Database loadDB() {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream("db.ser"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			Database d = (Database) ois.readObject();
			ois.close();
			return d;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public User2 getUser(String id) {
		return (User2) db.get("users").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addUser(String username, String pass, int level) {
		db.get("users").put(username, new User2(username, pass, level));
	}

	@SuppressWarnings("unchecked")
	public void addUser(String username, String pass, Person person) {
		db.get("users").put(username, new User2(username, pass, person));
	}

	public Student getStudent(String id) {
		return (Student) db.get("students").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addStudent(String name, String surname, String id) {
		db.get("students").put(id, new Student(id, name, surname));
		String username = (id + surname.charAt(0)).toLowerCase();
		addUser(username, username, getStudent(id));
	}

	public Tutor getTutor(String id) {
		return (Tutor) db.get("tutors").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addTutor(String name, String surname, String id) {
		db.get("tutors").put(id, new Tutor(id, name, surname));
		String username = (id + surname.charAt(0)).toLowerCase();
		addUser(username, username, getTutor(id));
	}

	public Lecturer getLecturer(String id) {
		return (Lecturer) db.get("lecturers").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addLecturer(String name, String surname, String id) {
		db.get("lecturers").put(id, new Lecturer(id, name, surname));
		String username = (id + surname.charAt(0)).toLowerCase();
		addUser(username, username, getLecturer(id));
	}

	public Admin getAdmin(String id) {
		return (Admin) db.get("admins").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addAdmin(String name, String surname, String id) {
		db.get("admins").put(id, new Admin(id, name, surname));
		String username = (id + surname.charAt(0)).toLowerCase();
		addUser(username, username, getAdmin(id));
	}

	public Course getCourse(String id) {
		return (Course) db.get("courses").get(id);
	}

	@SuppressWarnings("unchecked")
	public void addCourse(String courseName, String courseID) {
		db.get("courses").put(courseID, new Course(courseName, courseID));
	}

	@SuppressWarnings("unchecked")
	public void addCourse(String courseName, String shortName, String courseID) {
		db.get("courses").put(courseID,
				new Course(courseName, shortName, courseID));
	}

	@SuppressWarnings("unchecked")
	public Set<String> getUsers() {
		return db.get("users").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getStudents() {
		return db.get("students").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getTutors() {
		return db.get("tutors").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getLecturers() {
		return db.get("lecturers").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getAdmins() {
		return db.get("admins").keySet();
	}

	@SuppressWarnings("unchecked")
	public Set<String> getCourses() {
		return db.get("courses").keySet();
	}

}
