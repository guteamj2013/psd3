package com.groupj.psd3.db;


public class PopulateDB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Database db = Database.getNewDB();

		db.addCourse("Java Programming 2", "COMPSCI 2001");
		Course course = db.getCourse("COMPSCI 2001");
		db.addTutor("Test", "Tutor", "3333333");
		Tutor tutor = db.getTutor("3333333");
		db.addAdmin("Test", "Admin", "0000000");
		Admin admin = db.getAdmin("0000000");
		Lecture lecture = new Lecture("Wednesday", "10:00");
		db.addStudent("Test", "Student", "1111111");
		course.addLecture(lecture);
		db.addLecturer("Test", "Lecturer", "2222222");
		db.addUser("admin", "password", admin);
		admin.addCourse(course);
		course.addStudent(db.getStudent("1111111"));
		Lab lab = new Lab("A", "Wednesday", "13:00", 20);
		lab.setTutor(tutor);
		course.addLab(lab);
		course.addLecturer(db.getLecturer("2222222"));
		Database.saveDB();
	}

}
