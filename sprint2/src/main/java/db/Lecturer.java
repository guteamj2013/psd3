package com.groupj.psd3.db;

public class Lecturer extends Person {

	private static final long serialVersionUID = 1L;

	public Lecturer(String id, String name, String surname) {
		super(id, name, surname);
		email = name + "." + surname + "@glasgow.ac.uk";
	}

}
