package com.groupj.psd3.db;

public class Student extends Person {

	private static final long serialVersionUID = 1L;

	public Student(String id, String name, String surname) {
		super(id, name, surname);
		email = (id + surname.charAt(0)).toLowerCase() + "@student.gla.ac.uk";
	}

}
