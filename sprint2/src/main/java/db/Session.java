package com.groupj.psd3.db;

import java.util.Date;
import java.util.HashSet;

public class Session {
	private String description;
	private Date date;
	private HashSet<Student> present;
	private HashSet<Student> absent;
	private HashSet<Student> mv;
	private Person staff;
	private String location;

	public Session(String description, HashSet<Student> present,
			HashSet<Student> absent, HashSet<Student> mv, Person staff,
			Date date, String location) {
		this.date = date;
		this.present = present;
		this.absent = absent;
		this.mv = mv;
		this.staff = staff;
		this.description = description;
		this.location = location;
	}

	public Session(Date date, String description) {
		this.date = date;
		this.present = new HashSet<Student>();
		this.absent = new HashSet<Student>();
		this.mv = new HashSet<Student>();
		this.staff = null;
		this.description = description;
		this.location = "";
	}

	public Session(Date date) {
		this.date = date;
		this.present = new HashSet<Student>();
		this.absent = new HashSet<Student>();
		this.mv = new HashSet<Student>();
		this.staff = null;
		this.description = "";
		this.location = "";
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public HashSet<Student> getPresent() {
		return present;
	}

	public void setPresent(HashSet<Student> present) {
		this.present = present;
	}

	public HashSet<Student> getAbsent() {
		return absent;
	}

	public void setAbsent(HashSet<Student> absent) {
		this.absent = absent;
	}

	public HashSet<Student> getMv() {
		return mv;
	}

	public void setMv(HashSet<Student> mv) {
		this.mv = mv;
	}

	public void addAbsent(Student student) {
		absent.add(student);
	}

	public void addPresent(Student student) {
		present.add(student);
	}

	public void addMv(Student student) {
		mv.add(student);
	}

	public Person getStaff() {
		return staff;
	}

	public void setStaff(Person staff) {
		this.staff = staff;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
