package com.groupj.psd3.db;

import java.util.HashSet;

public class Tutor extends Person {

	private static final long serialVersionUID = 1L;
	private HashSet<Lab> labs;

	public Tutor(String id, String name, String surname) {
		super(id, name, surname);
		labs = new HashSet<Lab>();
		email = name + "." + surname + "@glasgow.ac.uk";
	}

	public HashSet<Lab> getLabs() {
		return labs;
	}

	public void setLabs(HashSet<Lab> labs) {
		this.labs = labs;
	}

	public void addLab(Lab lab) {
		labs.add(lab);
	}

	public boolean removeLab(Lab lab) {
		return labs.remove(lab);
	}

}
