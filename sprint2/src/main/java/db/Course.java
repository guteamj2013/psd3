package com.groupj.psd3.db;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Scanner;

public class Course implements Serializable {

	private static final long serialVersionUID = 1L;
	private String courseName;
	private String shortName;
	private String courseID;
	private HashSet<Lecture> lectures;
	private HashSet<Lab> labs;
	private HashSet<Student> students;
	private HashSet<Admin> admins;
	private HashSet<Lecturer> lecturers;
	private HashSet<Tutor> tutors;

	public Course(String courseName, String shortName, String courseID,
			HashSet<Lecture> lectures, HashSet<Lab> labs,
			HashSet<Student> students, HashSet<Admin> admins,
			HashSet<Lecturer> lecturers, HashSet<Tutor> tutors) {
		this.courseName = courseName;
		this.shortName = shortName;
		this.courseID = courseID;
		this.lectures = lectures;
		this.labs = labs;
		this.students = students;
		this.admins = admins;
		this.lecturers = lecturers;
		this.tutors = tutors;
	}

	public Course(String courseName, String shortName, String courseID) {
		this.courseName = courseName;
		this.shortName = shortName;
		this.courseID = courseID;
		this.lectures = new HashSet<Lecture>();
		this.labs = new HashSet<Lab>();
		this.students = new HashSet<Student>();
		this.admins = new HashSet<Admin>();
		this.lecturers = new HashSet<Lecturer>();
		this.tutors = new HashSet<Tutor>();
	}

	public Course(String courseName, String courseID) {
		this.courseName = courseName;
		this.shortName = genShortName(courseName);
		this.courseID = courseID;
		this.lectures = new HashSet<Lecture>();
		this.labs = new HashSet<Lab>();
		this.students = new HashSet<Student>();
		this.admins = new HashSet<Admin>();
		this.lecturers = new HashSet<Lecturer>();
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getCourseID() {
		return courseID;
	}

	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}

	public HashSet<Lecture> getLectures() {
		return lectures;
	}

	public void setLectures(HashSet<Lecture> lectures) {
		this.lectures = lectures;
	}

	public HashSet<Lab> getLabs() {
		return labs;
	}

	public void setLabs(HashSet<Lab> labs) {
		this.labs = labs;
	}

	public HashSet<Student> getStudents() {
		return students;
	}

	public void setStudents(HashSet<Student> students) {
		this.students = students;
	}

	public HashSet<Admin> getAdmins() {
		return admins;
	}

	public void setAdmins(HashSet<Admin> admins) {
		this.admins = admins;
	}

	public HashSet<Lecturer> getLecturers() {
		return lecturers;
	}

	public void setLecturers(HashSet<Lecturer> lecturers) {
		this.lecturers = lecturers;
	}

	public HashSet<Tutor> getTutors() {
		return tutors;
	}

	public void setTutors(HashSet<Tutor> tutors) {
		this.tutors = tutors;
	}

	private String genShortName(String courseName) {
		String shortName = "";
		Scanner s = new Scanner(courseName);
		while (s.hasNext())
			shortName = shortName + s.next().charAt(0);
		s.close();
		return shortName;
	}

	void addStudent(Student student, boolean db) {
		students.add(student);
	}

	public void addStudent(Student student) {
		addStudent(student, true);
		Database.getDB().getStudent(student.getId()).addCourse(this, true);

	}

	void addLecturer(Lecturer lecturer, boolean db) {
		lecturers.add(lecturer);
	}

	public void addLecturer(Lecturer lecturer) {
		addLecturer(lecturer, true);
		Database.getDB().getLecturer(lecturer.getId()).addCourse(this, true);

	}

	void addAdmin(Admin admin, boolean db) {
		admins.add(admin);
	}

	public void addAdmin(Admin admin) {
		addAdmin(admin, true);
		Database.getDB().getAdmin(admin.getId()).addCourse(this, true);

	}

	void addTutor(Tutor tutor, boolean db) {
		tutors.add(tutor);
	}

	public void addTutor(Tutor tutor) {
		addTutor(tutor, true);
		Database.getDB().getTutor(tutor.getId()).addCourse(this, true);

	}

	public void addLecture(Lecture lecture) {
		lectures.add(lecture);
		lecture.setCourse(this);
	}

	public void addLab(Lab lab) {
		labs.add(lab);
		lab.setCourse(this);
	}

}
