package com.groupj.psd3.db;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashSet;

public class Lecture implements Serializable {

	private static final long serialVersionUID = 1L;
	private LinkedHashSet<Session> sessions;
	private Course course;
	private String day;
	private String time;
	private Date startDate;
	private Date endDate;

	public Lecture(LinkedHashSet<Session> sessions, Course course, String day,
			String time, Date startDate, Date endDate) {
		super();
		this.sessions = sessions;
		this.course = course;
		this.day = day;
		this.time = time;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Lecture(String day, String time) {
		super();
		this.day = day;
		this.time = time;
		this.sessions = new LinkedHashSet<Session>();
	}

	public LinkedHashSet<Session> getSessions() {
		return sessions;
	}

	public void setSessions(LinkedHashSet<Session> sessions) {
		this.sessions = sessions;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
