package com.groupj.psd3.db;

import java.io.Serializable;

public class Admin extends Person implements Serializable {

	static final long serialVersionUID = 1L;

	public Admin(String id, String name, String surname) {
		super(id, name, surname);
		email = name + "." + surname + "@glasgow.ac.uk";
	}

}
