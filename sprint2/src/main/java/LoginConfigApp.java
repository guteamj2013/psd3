
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.thymeleaf.spring3.SpringTemplateEngine;
import org.thymeleaf.spring3.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;


@Configuration
@ComponentScan
@EnableAutoConfiguration
public class LoginConfigApp {
	
	
	
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}


	@Bean
	public ServletContextTemplateResolver templateResolver() {
	ServletContextTemplateResolver resolver = new ServletContextTemplateResolver();
	resolver.setPrefix("/WEB-INF/views/");
	resolver.setSuffix(".html");
	resolver.setTemplateMode("HTML5");
	resolver.setCacheable(false);
	return resolver;

	}

    @Bean
    SpringTemplateEngine engine() {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.setTemplateResolver(templateResolver());
        return engine;
    }

    @Bean
    ThymeleafViewResolver viewResolver() {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(engine());
        return viewResolver;
    }
	
	
    
    public static void main(String[] args) throws Exception {
        SpringApplication.run(LoginConfigApp.class, args);
    }
    
}
